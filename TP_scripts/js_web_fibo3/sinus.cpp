#include <cmath>

#define _USE_MATH_DEFINES

double sinus(double a, double b, double x) {
	//log.console(2*M_PI*(2*x+0.25));
	return sin(2*M_PI*(a*x+b));
}

#include <emscripten/bind.h>

EMSCRIPTEN_BINDINGS(sinus) {
    emscripten::function("sinus", &sinus);
}


