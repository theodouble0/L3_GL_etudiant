#include "Spline.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupSpline) { };

TEST(GroupSpline, test_fibo_hs) {
    try {
        Spline spline;
        spline.addKey(0, Vec2{0,0});
        spline.addKey(-1, Vec2{0,0});
        spline.addKey(2, Vec2{0,0});
        FAIL("l'exception n'est pas levée");
    }
    catch (const std::string & e) {
        std::string msg("les clés ne sont pas dans l'ordre croissant");
        CHECK(msg == e);
    }
    catch (...) {
        FAIL("l'exception n'est pas du bon type");
    }
}
