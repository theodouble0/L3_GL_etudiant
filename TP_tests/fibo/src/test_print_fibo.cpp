#include "Fibo.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibo){ };

TEST(GroupFibo, test1){
    //~ CHECK_THROWS(std::string, fibo(-1));
    CHECK_EQUAL(fibo(1), 1);
    CHECK_EQUAL(fibo(2) ,1);
    CHECK_EQUAL(fibo(3) ,2);
}

TEST(GroupFibo, test_fibo_hs) {
    try {
        int result = fibo(-1);
        FAIL("l'exception n'est pas levée");
    }
    catch (const std::string & e) {
        CHECK("beurk: n<0" == e);
    }
    catch (...) {
        FAIL("l'exception n'est pas du bon type");
    }
}
