#include "Fibo.hpp"
#include <cassert>
#include <iostream>

int fibo(int n, int f0, int f1) {
	if(n <0 ) throw std::string("beurk: n<0");
	assert(f0 < f1);
	assert(f0 <= 0);
    int r = n<=0 ? f0 : fibo(n-1, f1, f1+f0);
    assert(r >= 0);
    return r;
}

