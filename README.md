# L3_GL_etudiant

Code pour le module Génie Logiciel de L3 Informatique. Voir la [page du
module](https://juliendehos.gitlab.io/posts/gl/index.html).

# drunk_player

## Desription

blabla

- item 1
- item 2

## Dépendances

- opencv
- boost

## Utilisation

```bash
./drunk_player_gui.ou ../data
```

![](drunk_player_gui.png)

[dépôt gitlab](https://gitlab.com/theodouble0/cicd)

lien explicite : <https://gitlab.com>

code inline : `int main() {return 0;}`

*italique*

**gras**

> bloc
> de code
- item
- item
	- sous-item
	- sous-item
